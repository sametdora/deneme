﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DENEME
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Person person = new Person();
            person.Id = 1;
            person.FirstName = "Samet";
            person.LastName = "Dora";

            Person person1 = new Person();
            person1.Id = 2;
            person1.FirstName = "Sefa";
            person1.LastName = "Güven"; 



            Customer customer = new Customer();
            customer.Id = 20;
            customer.FirstName = "Ahmet";
            customer.LastName = "Demir";
            customer.CreditCardNumber = 1426317611;

            Person person2 = new Customer();
            person2.Id = 3 ;


            Employee employee = new Employee();
            employee.EmployeeNumber = 30;
            employee.FirstName = "Semih";
            employee.LastName = "Dora";


            PersonManager personManager = new PersonManager();
            personManager.Add(person2);


        }
        public class Person
        {
            public int Id { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }

        }
        public class Customer:Person
        {
            public int CreditCardNumber { get; set; }
        }
        public class Employee:Person
        {
            public int EmployeeNumber { get; set; }
        }

        public class PersonManager
        {
            public void Add(Person person)
            {
                Console.WriteLine(person.LastName);
                Console.ReadKey();
            }

        }

    }
}
